package com.iteaj.iboot.msn.core.service;

import com.iteaj.framework.IBaseService;
import com.iteaj.iboot.msn.core.entity.Region;

public interface IRegionService extends IBaseService<Region> {

}
