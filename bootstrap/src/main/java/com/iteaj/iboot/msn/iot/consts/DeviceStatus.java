package com.iteaj.iboot.msn.iot.consts;

public enum DeviceStatus {

    online, // 在线
    offline // 离线
}
