package com.iteaj.iboot.msn.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.iteaj.iboot.msn.core.entity.Config;
import org.apache.ibatis.annotations.Mapper;

/**
 * create time: 2019/12/4
 *
 * @author iteaj
 * @since 1.0
 */
@Mapper
public interface IConfigDao extends BaseMapper<Config> {

}
