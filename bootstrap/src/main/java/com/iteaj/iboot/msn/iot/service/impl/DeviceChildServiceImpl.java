package com.iteaj.iboot.msn.iot.service.impl;

import com.iteaj.iboot.msn.iot.entity.DeviceChild;
import com.iteaj.iboot.msn.iot.mapper.DeviceChildMapper;
import com.iteaj.iboot.msn.iot.service.IDeviceChildService;
import com.iteaj.framework.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 子设备 服务实现类
 * </p>
 *
 * @author iteaj
 * @since 2022-06-18
 */
@Service
public class DeviceChildServiceImpl extends BaseServiceImpl<DeviceChildMapper, DeviceChild> implements IDeviceChildService {

}
