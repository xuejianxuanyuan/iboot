package com.iteaj.iboot.msn.iot.mapper;

import com.iteaj.iboot.msn.iot.entity.DeviceType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 设备类型 Mapper 接口
 * </p>
 *
 * @author iteaj
 * @since 2022-05-15
 */
public interface DeviceTypeMapper extends BaseMapper<DeviceType> {

}
