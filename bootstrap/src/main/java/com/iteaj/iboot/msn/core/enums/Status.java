package com.iteaj.iboot.msn.core.enums;

/**
 * jdk：1.8
 *
 * @author iteaj
 * create time：2019/6/24
 */
public enum Status {
    enabled, disabled
}
