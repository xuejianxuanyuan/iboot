package com.iteaj.iboot.msn.iot.mapper;

import com.iteaj.iboot.msn.iot.entity.DeviceChild;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 子设备 Mapper 接口
 * </p>
 *
 * @author iteaj
 * @since 2022-06-18
 */
public interface DeviceChildMapper extends BaseMapper<DeviceChild> {

}
