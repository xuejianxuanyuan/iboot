package com.iteaj.iboot.msn.iot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.iteaj.iboot.msn.iot.entity.DeviceModel;

/**
 * <p>
 * 设备型号 Mapper 接口
 * </p>
 *
 * @author iteaj
 * @since 2022-07-22
 */
public interface DeviceModelMapper extends BaseMapper<DeviceModel> {

    IPage<DeviceModel> detailOfPage(Page<DeviceModel> page, DeviceModel entity);
}
