package com.iteaj.iboot.msn.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.iteaj.iboot.msn.core.entity.Region;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface IRegionDao extends BaseMapper<Region> {

}
