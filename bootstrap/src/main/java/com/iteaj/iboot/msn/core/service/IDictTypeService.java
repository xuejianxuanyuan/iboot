package com.iteaj.iboot.msn.core.service;

import com.iteaj.framework.IBaseService;
import com.iteaj.iboot.msn.core.entity.DictType;

/**
 * jdk：1.8
 *
 * @author iteaj
 * create time：2019/7/13
 */
public interface IDictTypeService extends IBaseService<DictType> {

}
