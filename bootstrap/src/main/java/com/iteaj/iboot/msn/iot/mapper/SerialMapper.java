package com.iteaj.iboot.msn.iot.mapper;

import com.iteaj.iboot.msn.iot.entity.Serial;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 串口设备 Mapper 接口
 * </p>
 *
 * @author iteaj
 * @since 2023-04-12
 */
public interface SerialMapper extends BaseMapper<Serial> {

}
