package com.iteaj.iboot.msn.iot.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.iteaj.framework.exception.ServiceException;
import com.iteaj.framework.result.BooleanResult;
import com.iteaj.framework.result.DetailResult;
import com.iteaj.framework.result.ListResult;
import com.iteaj.framework.result.NumberResult;
import com.iteaj.framework.utils.TreeUtils;
import com.iteaj.iboot.msn.iot.entity.DeviceModel;
import com.iteaj.iboot.msn.iot.entity.DeviceType;
import com.iteaj.iboot.msn.iot.mapper.DeviceTypeMapper;
import com.iteaj.iboot.msn.iot.service.IDeviceModelService;
import com.iteaj.iboot.msn.iot.service.IDeviceTypeService;
import com.iteaj.framework.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 设备类型 服务实现类
 * </p>
 *
 * @author iteaj
 * @since 2022-05-15
 */
@Service
public class DeviceTypeServiceImpl extends BaseServiceImpl<DeviceTypeMapper, DeviceType> implements IDeviceTypeService {

    @Autowired
    private IDeviceModelService deviceModelService;

    @Override
    public ListResult<DeviceType> tree(DeviceType entity) {
        List<DeviceType> types = this.list(Wrappers.lambdaQuery(entity)).ofNullable()
                .map(value -> TreeUtils.toTrees(value)
                        .stream().collect(Collectors.toList())).get();
        return new ListResult<>(types);
    }

    @Override
    public BooleanResult save(DeviceType entity) {
        super.save(entity);
        if(entity.getPid() != null && entity.getPid() != 0) {
            DeviceType parent = this.getById(entity.getPid())
                    .ofNullable().orElseThrow(() -> new ServiceException("父类型不存在"));
            entity.setPath(parent.getPath()+","+entity.getId());
        } else {
            entity.setPath(entity.getId().toString());
        }

        return this.update(Wrappers.<DeviceType>lambdaUpdate()
                .set(DeviceType::getPath, entity.getPath())
                .eq(DeviceType::getId, entity.getId()));
    }

    @Override
    public BooleanResult updateById(DeviceType entity) {
        if(entity.getPid() != null && entity.getPid() != 0) {
            DeviceType parent = this.getById(entity.getPid())
                    .ofNullable().orElseThrow(() -> new ServiceException("父类型不存在"));

            entity.setPath(parent.getPath()+","+entity.getId());
        } else {
            entity.setPath(entity.getId().toString());
        }
        return super.updateById(entity);
    }

    @Override
    public BooleanResult removeByIds(Collection<? extends Serializable> idList) {
        if(CollectionUtils.isEmpty(idList)) {
            return new BooleanResult(false);
        }

        NumberResult<Integer> count = deviceModelService.count(Wrappers
                .<DeviceModel>lambdaQuery().in(DeviceModel::getTypeId, idList));
        Integer integer = count.ofNullable().orElse(0);
        if(integer > 0) {
            throw new ServiceException("此设备类型已在设备型号中被使用");
        }

        return super.removeByIds(idList);
    }
}
