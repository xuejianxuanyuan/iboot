package com.iteaj.iboot.msn.core.service;

import com.iteaj.framework.IBaseService;
import com.iteaj.iboot.msn.core.entity.AccessLog;

/**
 * create time: 2020/4/22
 *
 * @author iteaj
 * @since 1.0
 */
public interface IAccessLogService extends IBaseService<AccessLog> {

}
