package com.iteaj.iboot.msn.quartz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.iteaj.iboot.msn.quartz.entity.JobTask;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface JobTaskMapper extends BaseMapper<JobTask> {

}
