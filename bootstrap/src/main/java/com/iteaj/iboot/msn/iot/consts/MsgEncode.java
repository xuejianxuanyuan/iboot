package com.iteaj.iboot.msn.iot.consts;

/**
 * 报文编码格式
 */
public enum MsgEncode {
    HEX,
    UTF8,
    GBK
}