package com.iteaj.iboot.msn.quartz.service;

import com.iteaj.framework.IBaseService;
import com.iteaj.iboot.msn.quartz.entity.JobTask;

public interface IJobTaskService extends IBaseService<JobTask> {

}
