package com.iteaj.iboot.msn.iot.consts;

public enum SerialStatus {

    open, // 打开串口
    close // 关闭串口
}
